import UIKit
import MobileCoreServices
import Photos

extension CMTime {
	static let zero = kCMTimeZero
	
	static func - (lhs: CMTime, rhs: TimeInterval) -> CMTime {
		return CMTime(seconds: lhs.seconds - rhs, preferredTimescale: lhs.timescale)
	}
}

extension CMTimeRange {
}

enum Bookend {
	case start
	case end
}

// Assumes that `instructionArray` is already valid for the composition's asset.
func insert(_ instructionToInsert: AVVideoCompositionInstruction, into instructionArray: [AVVideoCompositionInstruction]) -> [AVVideoCompositionInstruction] {
	return instructionArray.flatMap { (existingInstruction: AVVideoCompositionInstruction) -> [AVVideoCompositionInstruction] in
		let overlappingTimeRange = existingInstruction.timeRange.intersection(instructionToInsert.timeRange)
		if overlappingTimeRange.isEmpty {
			// No intersection - can use this instruction
			return [existingInstruction]
		} else if existingInstruction.timeRange.containsTimeRange(instructionToInsert.timeRange) || overlappingTimeRange == instructionToInsert.timeRange {
			// Either a single existing instruction completely contains the new instruction,
			// or the instruction to insert completely spans the existing instruction.

			// Make instructions to match the existing instruction that happens before and after the new instruction occurs.
			let existingInstructionPre = existingInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
			existingInstructionPre.timeRange = CMTimeRange(start: existingInstruction.timeRange.start, end: instructionToInsert.timeRange.start)
			let existingInstructionPost = existingInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
			existingInstructionPost.timeRange = CMTimeRange(start: instructionToInsert.timeRange.end, end: existingInstruction.timeRange.end)

			// Combine the existing and the new instructions for the overlap.
			let combinedInstruction = existingInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
			combinedInstruction.timeRange = instructionToInsert.timeRange
			combinedInstruction.layerInstructions =
				insert(instructionToInsert.layerInstructions, into: existingInstruction.layerInstructions)

			return [existingInstructionPre, combinedInstruction, existingInstructionPost]
		} else if overlappingTimeRange.start < instructionToInsert.timeRange.start {
			// New instruction begins after existing instruction, and ends after existing instruction.
			let existingInstructionPre = existingInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
			existingInstructionPre.timeRange = CMTimeRange(start: existingInstruction.timeRange.start, end: instructionToInsert.timeRange.start)

			let combinedInstruction = existingInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
			combinedInstruction.timeRange = CMTimeRange(start: instructionToInsert.timeRange.start, end: existingInstruction.timeRange.end)
			combinedInstruction.layerInstructions =
				insert(instructionToInsert.layerInstructions, into: existingInstruction.layerInstructions)

			return [existingInstructionPre, combinedInstruction]

		} else {
			// New instruction begins before existing instruction, and ends before existing instruction.
			let existingInstructionPost = existingInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
			existingInstructionPost.timeRange = CMTimeRange(start: instructionToInsert.timeRange.end, end: existingInstruction.timeRange.end)

			let combinedInstruction = existingInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
			combinedInstruction.timeRange = CMTimeRange(start: existingInstruction.timeRange.start, end: instructionToInsert.timeRange.end)
			combinedInstruction.layerInstructions =
				insert(instructionToInsert.layerInstructions, into: existingInstruction.layerInstructions)

			return [combinedInstruction, existingInstructionPost]
		}
	}.filter { !$0.timeRange.isEmpty }
}

func insert(_ instructionsToInsert: [AVVideoCompositionLayerInstruction], into instructionArray: [AVVideoCompositionLayerInstruction]) -> [AVVideoCompositionLayerInstruction] {
	// TODO: Make this better
//	return instructionArray + instructionsToInsert
	return instructionsToInsert
}

class ViewController: UIViewController {
	@IBOutlet weak var videoPlaceholderView: UIView! {
		didSet {
			playerLayer = AVPlayerLayer(player: player)
			playerLayer.frame = videoPlaceholderView.layer.bounds
			videoPlaceholderView.layer.addSublayer(playerLayer)
		}
	}

	@IBOutlet weak var progressBar: UISlider!
	
	let player = AVPlayer()
	var playerLayer: AVPlayerLayer!

	// Subtract this time from touchup time when editing.
	let editGestureCompensation: TimeInterval = 0.1
	
	var activeEditBuffer: (kind: Edit.Kind, startTime: CMTime)? = nil

	var isScrubbing = false
	
	struct Edit {
		enum Effect: String {
			case slowmo
		}

		enum Kind: Hashable {
			case cut
			case replay
			case zoom
			case effect(Effect)

			static func == (lhs: Kind, rhs: Kind) -> Bool {
				switch (lhs, rhs) {
				case (.cut, .cut):
					return true
				case (.replay, .replay):
					return true
				case (.zoom, .zoom):
					return true
				case let (.effect(leftEffect), .effect(rightEffect)):
					return leftEffect == rightEffect
				default:
					return false
				}
			}

			var hashValue: Int {
				switch self {
				case .cut:
					return "cut".hashValue
				case .replay:
					return "replay".hashValue
				case .zoom:
					return "zoom".hashValue
				case let .effect(effect):
					return "effect\(effect.hashValue)".hashValue
				}
			}
		}
		
		let kind: Kind
		var timeRange: CMTimeRange

		typealias Pipe = (videoTrack: AVMutableCompositionTrack, audioTrack: AVMutableCompositionTrack, videoComposition: AVMutableVideoComposition)

		func apply(to pipe: Pipe) -> Pipe {
			let (videoTrack, audioTrack, videoComposition) = pipe
			let compositionLayers = [videoTrack, audioTrack]

			switch kind {
			case .cut:
				compositionLayers.forEach { $0.removeTimeRange(timeRange) }
				
			case .replay:
				// TODO: try
				compositionLayers.forEach { try! $0.insertTimeRange(timeRange, of: $0, at: timeRange.end) }

			case .zoom:
				let zoomLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
				zoomLayerInstruction.setTransform(CGAffineTransform(scaleX: 2, y: 2), at: timeRange.start)
				let zoomInstruction = AVMutableVideoCompositionInstruction()
				zoomInstruction.layerInstructions = [zoomLayerInstruction]
				zoomInstruction.timeRange = timeRange

				videoComposition.instructions = insert(zoomInstruction, into: videoComposition.instructions.map { $0 as! AVVideoCompositionInstruction})

			case .effect(.slowmo):
				var stretchedDuration = timeRange.duration
				stretchedDuration.value *= 2

				compositionLayers.forEach { $0.scaleTimeRange(timeRange, toDuration: stretchedDuration) }
			}

			// these are all reference values, so...
			return pipe
		}
	}
	
	var baseVideoAsset: AVAsset?
	var edits: [Edit] = []

	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Load example video
		loadVideo(from: Bundle.main.url(forResource: "testVideo", withExtension: "mp4")!)

		let progressTimer =
			CADisplayLink(target: self, selector: #selector(ViewController.updateProgressBar))

		progressTimer.add(to: .main, forMode: .commonModes)
	}

	@objc private func updateProgressBar() {
		guard !isScrubbing else {
			return
		}

		guard let itemDuration = player.currentItem?.duration else {
			return
		}

		let progress = player.currentTime().seconds / itemDuration.seconds
		progressBar.value = Float(progress)
	}
	
	func loadVideo(from url: URL) {
		baseVideoAsset = AVURLAsset(url: url)
		edits = []
		updateComposition()
	}

	@discardableResult
	func updateComposition() -> (composition: AVComposition, videoComposition: AVVideoComposition) {
		guard let baseVideo = baseVideoAsset else {
			fatalError("Implement me")
		}

		let (composition, videoComposition) =
			makeComposition(baseVideo: baseVideo, edits: edits)
		let playerItem = AVPlayerItem(asset: composition)
		playerItem.videoComposition = videoComposition
		playerItem.audioTimePitchAlgorithm = .timeDomain
		player.replaceCurrentItem(with: playerItem)

		return (composition, videoComposition)
	}


	func makeComposition(baseVideo: AVAsset, edits: [Edit]) -> (AVComposition, AVVideoComposition) {
		let baseVideoTrack = baseVideo.tracks(withMediaType: .video).first!

		let composition = AVMutableComposition()
		let videoComposition = AVMutableVideoComposition(propertiesOf: baseVideo)
		
		guard 
			let videoTrack = composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid),
			let audioTrack = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
			else {
				fatalError("Implement me")
			}
		
		videoTrack.preferredTransform = baseVideoTrack.preferredTransform
		do {
			try videoTrack.insertTimeRange(CMTimeRange(start: .zero,
			                                            duration: baseVideo.duration),
			                                      of: baseVideoTrack,
			                                      at: .zero)
			try audioTrack.insertTimeRange(CMTimeRange(start: .zero,
			                                           duration: baseVideo.duration),
			                               of: baseVideo.tracks(withMediaType: .audio).first!,
			                               at: .zero)
			_ = edits.reduce((videoTrack: videoTrack, audioTrack: audioTrack, videoComposition: videoComposition), { pipe, edit in
				edit.apply(to: pipe)
			})
		} catch {
			print("Error creating composition: ", error.localizedDescription)
		}
		
		 let actualVideoComposition = AVMutableVideoComposition(propertiesOf: composition)
//		print(actualVideoComposition.instructions.map { "Instruction \($0.timeRange.start.seconds) - \($0.timeRange.end.seconds)"})
		_ = edits.forEach { edit in
			if case .zoom = edit.kind {
				edit.apply(to: (videoTrack: videoTrack, audioTrack: audioTrack, videoComposition: actualVideoComposition))
			}
		}

		

////		return (composition, videoComposition)
//		
//		let pre = (actualVideoComposition.instructions.first! as! AVVideoCompositionInstruction).mutableCopy() as! AVMutableVideoCompositionInstruction
//		let post = (actualVideoComposition.instructions.first! as! AVVideoCompositionInstruction).mutableCopy() as! AVMutableVideoCompositionInstruction
//
//		let zoomTimeRange = CMTimeRange(start: CMTime(seconds: 2, preferredTimescale: pre.timeRange.start.timescale),
//		                                end: CMTime(seconds: 5, preferredTimescale: pre.timeRange.start.timescale))
//		
//		
////		let zoomEdit = Edit(kind: .zoom, timeRange: zoomTimeRange)
////		zoomEdit.apply(to: (videoTrack: videoTrack, audioTrack: audioTrack, videoComposition: actualVideoComposition))
//		
//		pre.timeRange = CMTimeRange(start: pre.timeRange.start, end: zoomTimeRange.start)
//		post.timeRange = CMTimeRange(start: zoomTimeRange.end, end: composition.duration)
//
//		let zoomLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
//		zoomLayerInstruction.setTransform(CGAffineTransform(scaleX: 2, y: 2), at: zoomTimeRange.start)
//		let zoomInstruction = AVMutableVideoCompositionInstruction()
//		zoomInstruction.layerInstructions = [zoomLayerInstruction]
//		zoomInstruction.timeRange = zoomTimeRange
//		actualVideoComposition.instructions = insert(zoomInstruction, into: actualVideoComposition.instructions as! [AVVideoCompositionInstruction])
////		actualVideoComposition.instructions = [pre, zoomInstruction, post]
		
		let lastInstruction = actualVideoComposition.instructions.sorted(by: { (a, b) in a.timeRange.end > b.timeRange.end }).first! as! AVVideoCompositionInstruction
		
		let extendedLastInstruction = lastInstruction.mutableCopy() as! AVMutableVideoCompositionInstruction
		extendedLastInstruction.timeRange = CMTimeRange(start: lastInstruction.timeRange.start, end: kCMTimePositiveInfinity)
		
		actualVideoComposition.instructions[actualVideoComposition.instructions.index(where: { ($0 as! AVVideoCompositionInstruction) == lastInstruction })!] = extendedLastInstruction
		
		
		
		
		print(actualVideoComposition.instructions.map { "Instruction \($0.timeRange.start.seconds) - \($0.timeRange.end.seconds)"})
		print(actualVideoComposition.instructions)
		
		print("Is valid? ", actualVideoComposition.isValid(for: composition,
//		                                                   timeRange: CMTimeRange(start: .zero, duration: composition.duration),
			timeRange: CMTimeRange(start: .zero, duration: kCMTimePositiveInfinity),
		                                                   validationDelegate: self))
		
		return (composition, actualVideoComposition)

	}


	func endEdit() {
		guard let (kind, startTime) = activeEditBuffer else {
			fatalError("Implement me")
		}
		
		let endTime = player.currentTime()
		edits.append(.init(kind: kind, timeRange: CMTimeRange(start: startTime - editGestureCompensation, end: endTime - editGestureCompensation)))
	}

	func performEdit(_ bookend: Bookend, _ action: Edit.Kind) {
		switch bookend {
		case .start:
			activeEditBuffer = (kind: action, startTime: player.currentTime())

		case .end:
			guard case .some(kind: action, let startTime) = activeEditBuffer else {
				return
			}

			let endTime = player.currentTime()
			edits.append(.init(kind: action, timeRange: CMTimeRange(start: startTime - editGestureCompensation, end: endTime - editGestureCompensation)))
			
			updateComposition()
		}
	}

	@IBAction func openVideoPicker() {
		let imagePicker = UIImagePickerController()
		imagePicker.delegate = self
		imagePicker.mediaTypes = [kUTTypeMovie as String]
		present(imagePicker, animated: true)
	}

	@IBAction func beginScrubbing() {
		isScrubbing = true
	}

	@IBAction func endScrubbing() {
		isScrubbing = false
	}

	@IBAction func handleScrub(slider: UISlider) {
		guard let itemDuration = player.currentItem?.duration else {
			return
		}

		var targetTime = itemDuration
		targetTime.value = Int64(Float(itemDuration.value) * slider.value)
		player.seek(to: targetTime)
	}

	@IBAction func startCut() {
		performEdit(.start, .cut)
	}

	@IBAction func endCut() {
		performEdit(.end, .cut)
	}

	@IBAction func startZoom() {
		performEdit(.start, .zoom)
	}

	@IBAction func endZoom() {
		performEdit(.end, .zoom)
	}

	@IBAction func startSlowmo() {
		performEdit(.start, .effect(.slowmo))
	}

	@IBAction func endSlowmo() {
		performEdit(.end, .effect(.slowmo))
	}

	// Questionable
	@IBAction func insertReplay() {
		let endTime = player.currentTime()
		var startTime = endTime - 2
		if startTime.seconds < 0 {
			startTime = .zero
		}

		edits.append(.init(kind: .replay, timeRange: CMTimeRange(start: startTime, end: endTime)))

		// TODO: performEvent with a bookend doesn't make sense for replay (after the fact)
		// not certain the "after the fact" is going to stick around so
	}

	@IBAction func togglePlayPause() {
		player.play()
	}

	@IBAction func stopVideo() {
		player.seek(to: .zero)
		player.pause()
		updateComposition()
	}

	@IBAction func exportToCameraRoll() {
		let temporaryURL =
			FileManager.default.temporaryDirectory.appendingPathComponent("\(UUID().uuidString).mov")

		exportVideo(to: temporaryURL) {
			PHPhotoLibrary.shared().performChanges({ PHAssetCreationRequest.creationRequestForAssetFromVideo(atFileURL: temporaryURL) }) { completed, error in
				if completed {
					print("Video is saved!")
				} else {
					print("Error saving to library: ", error!.localizedDescription)
				}
			}
		}
	}

	func exportVideo(to url: URL, completion: (() -> Void)?) {
		let (composition, videoComposition) = updateComposition()

		guard let exporter = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality) else { 
			return
		}
		
		exporter.videoComposition = videoComposition
		exporter.outputURL = url
		exporter.outputFileType = .mov
		exporter.shouldOptimizeForNetworkUse = true

		exporter.exportAsynchronously {
			completion?()
		}
	}
}

extension ViewController: UINavigationControllerDelegate {}

extension ViewController: UIImagePickerControllerDelegate {
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		dismiss(animated: true, completion: nil)
		
		info[UIImagePickerControllerMediaURL]
			.flatMap { $0 as? URL }
			.map { loadVideo(from: $0) }
 	}
}

extension ViewController: AVVideoCompositionValidationHandling {
	func videoComposition(_ videoComposition: AVVideoComposition, shouldContinueValidatingAfterFindingInvalidValueForKey key: String) -> Bool {
		print("Invalid value for key \(key)")
		return true
	}
	
	func videoComposition(_ videoComposition: AVVideoComposition, shouldContinueValidatingAfterFindingEmptyTimeRange timeRange: CMTimeRange) -> Bool {
		print("Empty time range \(timeRange) \(timeRange.start.seconds, timeRange.end.seconds)")
		return true
	}
	
	func videoComposition(_ videoComposition: AVVideoComposition, shouldContinueValidatingAfterFindingInvalidTimeRangeIn videoCompositionInstruction: AVVideoCompositionInstructionProtocol) -> Bool {
		print("Invalid time range in \(videoCompositionInstruction)")
		return true
	}
	func videoComposition(_ videoComposition: AVVideoComposition, shouldContinueValidatingAfterFindingInvalidTrackIDIn videoCompositionInstruction: AVVideoCompositionInstructionProtocol, layerInstruction: AVVideoCompositionLayerInstruction, asset: AVAsset) -> Bool {
		print("Invalid track ID in \(videoCompositionInstruction), \(layerInstruction), \(asset)")
		return true
	}
}
