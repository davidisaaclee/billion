# AVVideoComposition / AVComposition
- AVComposition
	- handles temporally coordinating multiple assets
	- has a set of tracks (think iMovie)
	- composition tracks (AVCompositionTrack / AVMutableCompositionTrack) contain a list of "segments", which are instructions to play a specified piece of media (asset track or nothing) at a specific time range.

- AVVideoComposition
	- can apply CIFilters to a video (but only by registering them at init?)
	- handles spatially coordinating one or more video sources
	- passed separately from AVComposition to exporter or player
	- can also do animation stuff? with Core Animation?
	- contains a list of AVVideoCompositionInstructions 
		- list must be non-empty (starts with a single "do nothing" instruction that spans the asset)
		- instructions can't overlap; and instructions must fill the duration of the asset (no gaps)
		- union of all AVVideoCompositionInstructions.timeRange in a video composition must be ≥ the asset's duration (otherwise will show blank video)
		- manages background color for the video
		- contains a list of AVVideoCompositionLayerInstructions
			- list must be non-empty; otherwise will show a black rect for video
			- layer instruction maps to and affects an AVAssetTrack (so i guess it's totally decoupled from AVComposition)
			- controls opacity, transform, crop

- AVAssetTrack
	- interface to an AVAsset's "tracks" of media - a .mov could have a video track and an audio track, for example.
	- but tracks in a composition are also AVAssetTracks... whaaaa


seems like a lucid example of adopting AVVideoCompositing and AVVideoCompositionInstructionProtocol
still not clear for what (non-bugfix) reason you would adopt AVVideoCompositionInstructionProtocol...
https://github.com/claygarrett/CustomVideoCompositor
